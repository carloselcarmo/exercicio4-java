package br.com.Itau;

import java.util.Map;

public class Main {

    public static void main(String[] args)
    {
        Map<String, String> dadosJogador = IO.solicitarNome();
        Jogador jogador = new Jogador(dadosJogador.get("nome"));

        Jogo21 jogo = new Jogo21( jogador, new Baralho());

        jogo.exibirMenu();
    }
}
