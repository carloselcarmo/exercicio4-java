package br.com.Itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO
{
    public static void exibirMensagemPerdeu (String nome)
    {
        System.out.println(nome + ", você perdeu!");
    }

    public static void exibirMensagemGanhou (String nome)
    {
        System.out.println(nome + ", você ganhou!");
    }

    public static void exibirMensagemParou (String nome, Rodada rodada)
    {
        System.out.println(nome + ", você parou com " + rodada.somarPontos() + " pontos");
    }

    public static void exibirMensagemNaoHaMelhorRodada ()
    {
        System.out.println("Ainda não há uma rodada computada com êxito");
    }

    public static void exibirCartas (Rodada rodada)
    {
        for (Carta carta : rodada.getCartasSorteadas())
        {
            System.out.print(carta.getSimbolo() + " de " + carta.getNaipe() );
            System.out.print(" ");
        }
        System.out.println("");
        System.out.println("Total de pontos: " + rodada.somarPontos());
    }

    public static void exibirMelhorRodada(Rodada rodada)
    {
        System.out.print("Melhor Rodada: ");
        exibirCartas(rodada);
    }

    public static Map<String, String> solicitarNome()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Jogador, qual seu nome? ");
        String nome = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);

        return dados;
    }

    public static Map<String, Integer> solicitarOpcaoJogo()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("O que você deseja fazer? ");
        System.out.printf("(1) Solicitar Nova Carta");
        System.out.printf("(2) Parar");
        Integer opcao = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("opcao", opcao);

        return dados;
    }

    public static Map<String, Integer> solicitarOpcoesDoJogo()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Selecione:");
        System.out.printf("(1) Novo jogo");
        System.out.printf("(2) Exibir Melhor Rodada");
        System.out.printf("(3) Sair do jogo");
        Integer opcao = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("opcao", opcao);

        return dados;
    }
}
