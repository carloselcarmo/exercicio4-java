package br.com.Itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Jogo21
{
    private Jogador jogador;
    private Rodada rodada;
    private Baralho baralho;
    private List<Rodada> historico;
    private Rodada melhorRodada;

    public Jogador getJogador()
    {
        return jogador;
    }

    public Baralho getBaralho()
    {
        return baralho;
    }

    public Jogo21(Jogador jogador, Baralho baralho)
    {
        this.jogador = jogador;
        this.baralho = baralho;
        this.rodada = new Rodada(jogador, baralho);
        this.historico  = new ArrayList<>();
    }

    private void parar()
    {
        if(melhorRodada == null)
            melhorRodada = rodada;
        else if(rodada.somarPontos() > melhorRodada.somarPontos())
            melhorRodada = rodada;
        historico.add(rodada);
        IO.exibirMensagemParou(jogador.getNome(), rodada);
    }

    private void exibirMelhorRodada()
    {
        if(melhorRodada != null)
            IO.exibirMelhorRodada(melhorRodada);
        else
            IO.exibirMensagemNaoHaMelhorRodada();
    }

    public void exibirMenu()
    {
        Integer opcaoJogo;
        do
        {
            Map<String, Integer> dadosOpcaoJogo = IO.solicitarOpcoesDoJogo();
            opcaoJogo = dadosOpcaoJogo.get("opcao");

            if(opcaoJogo == 1)
            {
                iniciarNovoJogo();
            }
            else if(opcaoJogo == 2)
            {
                exibirMelhorRodada();
            }
        } while(opcaoJogo != 3);
    }

    private void iniciarNovoJogo()
    {
        baralho.embaralhar();
        this.rodada = new Rodada(jogador, baralho);
        rodada.sortearCartasIniciais();
        rodada.imprimir();

        Integer opcaoJogo;
        do
        {
            Map<String, Integer> dadosOpcaoJogo = IO.solicitarOpcaoJogo();
            opcaoJogo = dadosOpcaoJogo.get("opcao");

            if(opcaoJogo == 1)
            {
                rodada.solicitarCarta();
                rodada.imprimir();
                ResultadoJogo resultado =  rodada.verificarResultado();
                if(resultado == ResultadoJogo.Ganhou)
                {
                    melhorRodada = rodada;
                    historico.add(rodada);
                    opcaoJogo = 2;
                }
                else if (resultado == ResultadoJogo.Perdeu)
                {
                    historico.add(rodada);
                    opcaoJogo = 2;
                }
            }
            else if(opcaoJogo == 2)
            {
                parar();
            }
        } while(opcaoJogo != 2);
    }
}
