package br.com.Itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Baralho
{
    List<Carta> cartas;

    public Baralho()
    {
        embaralhar ();
    }

    public void embaralhar ()
    {
        cartas = new ArrayList<>();

        for (Naipe naipe : Naipe.values())
        {
            for (Simbolo simbolo : Simbolo.values())
            {
                cartas.add(new Carta(simbolo, naipe));
            }
        }
    }

    public Carta sortear()
    {
        Random random = new Random();

        //Sorteia a carta do baralho
        Carta cartaSorteada = cartas.get(random.nextInt(cartas.size()));

        //Remove do baralho a carta sorteada
        cartas.remove(cartaSorteada);

        return cartaSorteada;
    }
}
