package br.com.Itau;

public enum Simbolo
{
    As (1),
    Dois(2),
    Tres(3),
    Quatro(4),
    Cinco(5),
    Seis(6),
    Sete(7),
    Oito(8),
    Nove(9),
    Dez(10),
    Valete(10),
    Dama(10),
    Rei(10);

    private int valor;


    public int getValor()
    {
        return valor;
    }

    Simbolo(int valor)
    {
        this.valor = valor;
    }
}
