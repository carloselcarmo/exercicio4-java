package br.com.Itau;

public class Carta
{
    private Simbolo simbolo;
    private Naipe naipe;

    public Simbolo getSimbolo()
    {
        return simbolo;
    }

    public Naipe getNaipe()
    {
        return naipe;
    }

    public Carta(Simbolo simbolo, Naipe naipe)
    {
        this.simbolo = simbolo;
        this.naipe = naipe;
    }
}
