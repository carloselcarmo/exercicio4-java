package br.com.Itau;

import java.util.ArrayList;
import java.util.List;

public class Rodada
{
    private List<Carta> cartasSorteadas;
    private Baralho baralho;
    private Jogador jogador;

    public List<Carta> getCartasSorteadas()
    {
        return cartasSorteadas;
    }

    public Rodada(Jogador jogador, Baralho baralho)
    {
        cartasSorteadas = new ArrayList<>();
        this.baralho = baralho;
        this.jogador = jogador;
    }

    public void sortearCartasIniciais()
    {
        solicitarCarta();
        solicitarCarta();
    }

    public void solicitarCarta()
    {
        cartasSorteadas.add(baralho.sortear());
    }

    public void imprimir()
    {
        IO.exibirCartas(this);
    }

    public ResultadoJogo verificarResultado()
    {
        Integer somaCartas = somarPontos();
        if(somaCartas > 21)
        {
            IO.exibirMensagemPerdeu(jogador.getNome());
            return ResultadoJogo.Perdeu;
        }
        else if (somaCartas == 21)
        {
            IO.exibirMensagemGanhou(jogador.getNome());
            return ResultadoJogo.Ganhou;
        }
        return ResultadoJogo.ContinuaJogo;
    }

    public Integer somarPontos()
    {
        Integer soma = 0;
        for (Carta carta: cartasSorteadas)
        {
            soma += carta.getSimbolo().getValor();
        }
        return soma;
    }
}
