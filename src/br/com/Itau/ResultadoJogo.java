package br.com.Itau;

public enum ResultadoJogo
{
    Ganhou,
    Perdeu,
    ContinuaJogo,
    Parou
}
